/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, ScrollView}  from 'react-native';  
import RootContainer from './src/RootContainer';
import { AdMobBanner, AdMobInterstitial } from 'react-native-google-admob';
import HeaderIn from './src/components/HeaderIn';

console.disableYellowBox = true;

export default class App extends Component<Props> {

  componentDidMount(){
    this.refresh((result) => {this.state.splash = result})
    // this.timer = setInterval(()=> this.getMovies(), 45000)
 
 }

  getMovies () {
      AdMobInterstitial.setAdUnitID('ca-app-pub-9534097551670907/6209331342');
      AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
      AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
  }

   //LOGIC: Send a http request and use the response to determine the connectivity state
   refresh(callback){
    httpAddress = 'https://www.google.com' //the site i'm building the app for
    var xhr = new XMLHttpRequest();
    xhr.open('GET', httpAddress);
    xhr.onreadystatechange = (e) => {
      if (xhr.readyState !== 4) { //code for completed request
        return;
      }
    //  alert("--- STATUS ---");
      //alert(xhr.status);
      if (xhr.status === 200) { //successful response
      //  callback(true);
      //  alert('OK ' + true)
      } else {                  //unsuccessful response
        /* NOTE: React native often reacts strangely to offline uses and as such,
        it may be necessary to directly set state here rather than to rely on a callback */
      //  callback(false)  //OR: this.state.splash = false

        alert('Sem conexão')
      }
    };
    xhr.send();
}

  render() {
       
    return (
      <View style={{flex: 1}}>
        <HeaderIn />
      <ScrollView>
      <RootContainer></RootContainer>
       <AdMobBanner
          adSize="fullBanner"
          adUnitID="ca-app-pub-9534097551670907/3227944781"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
        /> 
        </ScrollView>
        </View>
    );
  }
}
