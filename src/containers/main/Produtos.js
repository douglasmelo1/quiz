let Produtos = [
  {     pergunta: "Normalmente, quantos litros de sangue uma pessoa tem? Em média, quantos são retirados numa doação de sangue?",

resposta1: "Tem entre 2 a 4 litros. São retirados 450 mililitros",
resposta2: "Tem entre 4 a 6 litros. São retirados 450 mililitros",
resposta3: "Tem 10 litros. São retirados 2 litros",
resposta4: "Tem 7 litros. São retirados 1,5 litros",
resposta5: "Tem 0,5 litros. São retirados 0,5 litros",

respostacerta: "Tem entre 4 a 6 litros. São retirados 450 mililitros"},   {     pergunta: "De quem é a famosa frase “Penso, logo existo”?",

resposta1: "Platão",
resposta2: "Galileu Galilei",
resposta3: "Descartes",
resposta4: "Sócrates",
resposta5: "Francis Bacon",


respostacerta: "Descartes"},   {     pergunta: "De onde é a invenção do chuveiro elétrico?",

resposta1: "França",
resposta2: "Inglaterra",
resposta3: "Brasil",
resposta4: "Austrália",
resposta5: "Itália",


respostacerta: "Brasil"},   {     pergunta: "Quais o menor e o maior país do mundo?",

resposta1: "Vaticano e Rússia",
resposta2: "Nauru e China",
resposta3: "Mônaco e Canadá",
resposta4: "Malta e Estados Unidos",
resposta5: "São Marino e Índia",


respostacerta: "Vaticano e Rússia"},   {     pergunta: "Qual o nome do presidente do Brasil que ficou conhecido como Jango?",

resposta1: "Jânio Quadros",
resposta2: "Jacinto Anjos",
resposta3: "Getúlio Vargas",
resposta4: "João Figueiredo",
resposta5: "João Goulart",


respostacerta: "João Goulart"},   {     pergunta: "Qual o grupo em que todas as palavras foram escritas corretamente?",

resposta1: "Asterístico, beneficiente, meteorologia, entertido",
resposta2: "Asterisco, beneficente, meteorologia, entretido",
resposta3: "Asterisco, beneficente, metereologia, entretido",
resposta4: "Asterístico, beneficiente, metereologia, entretido",
resposta5: "Asterisco, beneficiente, metereologia, entretido",


respostacerta: "Asterisco, beneficente, meteorologia, entretido"},   {     pergunta: "Qual o livro mais vendido no mundo a seguir à Bíblia?",

resposta1: "O Senhor dos Anéis",
resposta2: "Dom Quixote",
resposta3: "O Pequeno Príncipe",
resposta4: "Ela, a Feiticeira",
resposta5: "Um Conto de Duas Cidades",


respostacerta: "Dom Quixote"},   {     pergunta: "Quantas casas decimais tem o número pi?",

resposta1: "Duas",
resposta2: "Centenas",
resposta3: "Trilhares",
resposta4: "Vinte",
resposta5: "Milhares",


respostacerta: "Trilhares"},   {     pergunta: "Atualmente, quantos elementos químicos a tabela periódica possui?",

resposta1: "113",
resposta2: "109",
resposta3: "108",
resposta4: "118",
resposta5: "92",


respostacerta: "118"},   {     pergunta: "Quais os países que têm a maior e a menor expectativa de vida do mundo?",

resposta1: "Japão e Serra Leoa",
resposta2: "Austrália e Afeganistão",
resposta3: "Itália e Chade",
resposta4: "Brasil e Congo",
resposta5: "Estados Unidos e Angola",


respostacerta: "Japão e Serra Leoa"},   {     pergunta: "O que a palavra legend significa em português?",

resposta1: "Legenda",
resposta2: "Conto",
resposta3: "História",
resposta4: "Lenda",
resposta5: "Legendário",



respostacerta: ""},   {     pergunta: "Qual o número mínimo de jogadores numa partida de futebol?",

resposta1: "8",
resposta2: "10",
resposta3: "9",
resposta4: "5",
resposta5: "7",


respostacerta: "7"},   {     pergunta: "Quais os principais autores do Barroco no Brasil?",

resposta1: "Gregório de Matos, Bento Teixeira e Manuel Botelho de Oliveira",
resposta2: "Miguel de Cervantes, Gregório de Matos e Danthe Alighieri",
resposta3: "Padre Antônio Vieira, Padre Manuel de Melo e Gregório de Matos",
resposta4: "Castro Alves, Bento Teixeira e Manuel Botelho de Oliveira",
resposta5: "Álvares de Azevedo, Gregório de Matos e Bento Teixeira",


respostacerta: "Gregório de Matos, Bento Teixeira e Manuel Botelho de Oliveira"},   {     pergunta: "Quais as duas datas que são comemoradas em novembro?",

resposta1: "Independência do Brasil e Dia da Bandeira",
resposta2: "Proclamação da República e Dia Nacional da Consciência Negra",
resposta3: "Dia do Médico e Dia de São Lucas",
resposta4: "Dia de Finados e Dia Nacional do Livro",
resposta5: "Black Friday e Dia da Árvore",


respostacerta: "Proclamação da República e Dia Nacional da Consciência Negra"},   {     pergunta: "Quem pintou 'Guernica'?",

resposta1: "Paul Cézanne",
resposta2: "Pablo Picasso",
resposta3: "Diego Rivera",
resposta4: "Tarsila do Amaral",
resposta5: "Salvador Dalí",


respostacerta: "Pablo Picasso"},   {     pergunta: "Quanto tempo a luz do Sol demora para chegar à Terra?",

resposta1: "12 minutos",
resposta2: "1 dia",
resposta3: "12 horas",
resposta4: "8 minutos",
resposta5: "segundos",


respostacerta: "8 minutos"},   {     pergunta: "Qual a tradução da frase “Fabiano cogió su saco antes de salir”?",

resposta1: "Fabiano coseu seu paletó antes de sair",
resposta2: "Fabiano fechou o saco antes de sair",
resposta3: "Fabiano pegou seu paletó antes de sair",
resposta4: "Fabiano cortou o saco antes de cair",
resposta5: "Fabiano rasgou seu paletó antes de cair",


respostacerta: "Fabiano pegou seu paletó antes de sair"},   {     pergunta: "Qual a nacionalidade de Che Guevara?",

resposta1: "Cubana",
resposta2: "Peruana",
resposta3: "Panamenha",
resposta4: "Boliviana",
resposta5: "Argentina",


respostacerta: "Argentina"},   {     pergunta: "Quais são os três predadores do reino animal reconhecidos pela habilidade de caçar em grupo, se camuflar para surpreender as presas e possuir sentidos apurados, respectivamente:",

resposta1: "Tubarão branco, crocodilo e sucuri",
resposta2: "Tigre, gavião e orca",
resposta3: "Hiena, urso branco e lobo cinzento",
resposta4: "Orca, onça e tarântula",
resposta5: "Leão, tubarão branco e urso cinzento",


respostacerta: "Hiena, urso branco e lobo cinzento"},   {     pergunta: "Qual a altura da rede de vôlei nos jogos masculino e feminino?",

resposta1: "2,4 para ambos",
resposta2: "2,5 m e 2,0 m",
resposta3: "1,8 m e 1,5 m",
resposta4: "2,45 m e 2,15 m",
resposta5: "2,43 m e 2,24 m",


respostacerta: "2,43 m e 2,24 m"},   {     pergunta: "Em que ordem surgiram os modelos atômicos?",

resposta1: "Thomson, Dalton, Rutherford, Rutherford-Bohr",
resposta2: "Rutherford-Bohr, Rutherford, Thomson, Dalton",
resposta3: "Dalton, Rutherford-Bohr, Thomson, Rutherford",
resposta4: "Dalton, Thomson, Rutherford-Bohr, Rutherford",
resposta5: "Dalton, Thomson, Rutherford, Rutherford-Bohr",


respostacerta: "Dalton, Thomson, Rutherford, Rutherford-Bohr"},   {     pergunta: "Qual personagem folclórico costuma ser agradado pelos caçadores com a oferta de fumo?",

resposta1: "Caipora",
resposta2: "Saci",
resposta3: "Lobisomem",
resposta4: "Boitatá",
resposta5: "Negrinho do Pastoreio",



respostacerta: "Caipora"},   {     pergunta: "Em que período da pré-história o fogo foi descoberto?",

resposta1: "Neolítico",
resposta2: "Paleolítico",
resposta3: "Idade dos Metais",
resposta4: "Período da Pedra Polida",
resposta5: "Idade Média",


respostacerta: "Paleolítico"},   {     pergunta: "Qual das alternativas abaixo apenas contêm classes de palavras?",

resposta1: "Vogais, semivogais e consoantes",
resposta2: "Artigo, verbo transitivo e verbo intransitivo",
resposta3: "Fonologia, Morfologia e Sintaxe",
resposta4: "Hiatos, ditongos e tritongos",
resposta5: "Substantivo, verbo e preposição",


respostacerta: "Substantivo, verbo e preposição"},   {     pergunta: "Qual a montanha mais alta do Brasil?",

resposta1: "Pico da Neblina",
resposta2: "Pico Paraná",
resposta3: "Monte Roraima",
resposta4: "Pico Maior de Friburgo",
resposta5: "Pico da Bandeira",


respostacerta: "Pico da Neblina"},   {     pergunta: "Qual a velocidade da luz?",

resposta1: "300 000 000 metros por segundo (m/s)",
resposta2: "150 000 000 metros por segundo (m/s)",
resposta3: "199 792 458 metros por segundo (m/s)",
resposta4: "299 792 458 metros por segundo (m/s)",
resposta5: "30 000 000 metros por segundo (m/s)",


respostacerta: "299 792 458 metros por segundo (m/s)"},   {     pergunta: "Em qual local da Ásia o português é língua oficial?",

resposta1: "Índia",
resposta2: "Filipinas",
resposta3: "Moçambique",
resposta4: "Macau",
resposta5: "Portugal",


respostacerta: "Macau"},   {     pergunta: "“It is six twenty ou twenty past six”. Que horas são em inglês?",

resposta1: "12:06",
resposta2: "6:20",
resposta3: "2:20",
resposta4: "6:02",
resposta5: "12:20",


respostacerta: "6:20"},   {     pergunta: "Quem é o autor de “O Príncipe”?",

resposta1: "Maquiavel",
resposta2: "Antoine de Saint-Exupéry",
resposta3: "Montesquieu",
resposta4: "Thomas Hobbes",
resposta5: "Rousseau",


respostacerta: "Maquiavel"},   {     pergunta: "Como é a conjugação do verbo caber na 1.ª pessoa do singular do presente do indicativo?",

resposta1: "Eu caibo",
resposta2: "Ele cabe",
resposta3: "Que eu caiba",
resposta4: "Eu cabo",
resposta5: "Nenhuma das alternativas",


respostacerta: "Eu caibo"},   {     pergunta: "Quais destas construções famosas ficam nos Estados Unidos?",

resposta1: "Estátua da Liberdade, Golden Gate Bridge e Empire State Building",
resposta2: "Estátua da Liberdade, Big Ben e The High Line",
resposta3: "Angkor Wat, Taj Mahal e Skywalk no Grand Canyon",
resposta4: "Lincoln Memorial, Sidney Opera House e Burj Khalifa",
resposta5: "30 St Mary Axe, The High Line e Residencial 148 Spruce Street",


respostacerta: "Estátua da Liberdade, Golden Gate Bridge e Empire State Building"},   {     pergunta: "Quais destas doenças são sexualmente transmissíveis?",

resposta1: "Aids, tricomoníase e ebola",
resposta2: "Chikungunya, aids e herpes genital",
resposta3: "Gonorreia, clamídia e sífilis",
resposta4: "Botulismo, cistite e gonorreia",
resposta5: "Hepatite B, febre tifoide e hanseníase",

respostacerta: "Gonorreia, clamídia e sífilis"},   {     pergunta: "Qual destes países é transcontinental?",

resposta1: "Rússia",
resposta2: "Filipinas",
resposta3: "Istambul",
resposta4: "Groenlândia",
resposta5: "Tanzânia",


respostacerta: "Rússia"},   {     pergunta: "Em qual das orações abaixo a palavra foi empregada incorretamente?",

resposta1: "Mais uma vez, portou-se mal.",
resposta2: "É um homem mal.",
resposta3: "Esse é o mal de todos.",
resposta4: "Mal falou nele, o fulano apareceu.",
resposta5: "É um mau vendedor.",



respostacerta: "É um homem mal."},   {     pergunta: "Qual foi o recurso utilizado inicialmente pelo homem para explicar a origem das coisas?",

resposta1: "A Filosofia",
resposta2: "A Biologia",
resposta3: "A Matemática",
resposta4: "A Astronomia",
resposta5: "A Mitologia",


respostacerta: "A Mitologia"},   {     pergunta: "Qual das alternativas menciona apenas símbolos nacionais?",

resposta1: "Bandeira insígnia da presidência, bandeira nacional, brasão, hinos e selo",
resposta2: "Bandeira nacional, armas nacionais, hino nacional e selo nacional",
resposta3: "Bandeira nacional, brasão, hino nacional e hino da independência",
resposta4: "Bandeira nacional, cores nacionais, hino nacional e hino da independência",
resposta5: "Bandeira insígnia da presidência, brasão flora e fauna e hinos",


respostacerta: "Bandeira nacional, armas nacionais, hino nacional e selo nacional"},   {     pergunta: "Quais os planetas do sistema solar?",

resposta1: "Terra, Vênus, Saturno, Urano, Júpiter, Marte, Netuno, Mercúrio",
resposta2: "Júpiter, Marte, Mercúrio, Netuno, Plutão, Saturno, Terra, Urano, Vênus",
resposta3: "Vênus, Saturno, Urano, Júpiter, Marte, Netuno, Mercúrio",
resposta4: "Júpiter, Marte, Mercúrio, Netuno, Plutão, Saturno, Sol, Terra, Urano, Vênus",
resposta5: "Terra, Vênus, Saturno, Júpiter, Marte, Netuno, Mercúrio",


respostacerta: "Terra, Vênus, Saturno, Urano, Júpiter, Marte, Netuno, Mercúrio"},   {     pergunta: "Qual era o nome de Aleijadinho?",

resposta1: "Alexandrino Francisco Lisboa",
resposta2: "Manuel Francisco Lisboa",
resposta3: "Alex Francisco Lisboa",
resposta4: "Francisco Antônio Lisboa",
resposta5: "Antônio Francisco Lisboa",


respostacerta: "Antônio Francisco Lisboa"},   {     pergunta: "Júpiter e Plutão são os correlatos romanos de quais deuses gregos?",

resposta1: "Ares e Hermes",
resposta2: "Cronos e Apolo",
resposta3: "Zeus e Hades",
resposta4: "Dionísio e Deméter",
resposta5: "Zeus e Ares",


respostacerta: "Zeus e Hades"},   {     pergunta: "Qual o maior animal terrestre?",

resposta1: "Baleia Azul",
resposta2: "Dinossauro",
resposta3: "Elefante africano",
resposta4: "Tubarão Branco",
resposta5: "Girafa",


respostacerta: "Elefante africano"},   {     pergunta: "Qual o tema do famoso discurso Eu Tenho um Sonho, de Martin Luther King?",

resposta1: "Igualdade das raças",
resposta2: "Justiça para os menos favorecidos",
resposta3: "Intolerância religiosa",
resposta4: "Prêmio Nobel da Paz",
resposta5: "Luta contra o Apartheid",


respostacerta: "Igualdade das raças"},   {     pergunta: "Que líder mundial ficou conhecida como “Dama de Ferro”?",

resposta1: "Dilma Rousseff",
resposta2: "Angela Merkel",
resposta3: "Margaret Thatcher",
resposta4: "Hillary Clinton",
resposta5: "Christine Lagarde",


respostacerta: "Margaret Thatcher"},   
{     pergunta: "O que são Acordo de Paris e Tríplice Aliança respectivamente?",

resposta1: "Acordo ortográfico entre países cuja língua oficial é o francês e Acordo de cooperação financeira internacional entre as três maiores potências mundiais",
resposta2: "Acordo entre países europeus acerca da imigração e Acordo econômico entre Inglaterra, Rússia a França",
resposta3: "Acordo entre vários países acerca das consequências do aquecimento global e Acordo de cooperação financeira internacional entre as três maiores potências mundiais",
resposta4: "Acordo de cooperação financeira internacional entre as três maiores potências mundiais e Acordo entre vários países acerca das consequências do aquecimento global",
resposta5: "Acordo entre vários países acerca das consequências do aquecimento global e Acordo entre Alemanha, império Austro-Húngaro e Itália acerca de apoio em caso de guerra",


respostacerta: "Acordo entre vários países acerca das consequências do aquecimento global e Acordo entre Alemanha, império Austro-Húngaro e Itália acerca de apoio em caso de guerra"},   

{     pergunta: "Quais os nomes dos três Reis Magos?",

resposta1: "Gaspar, Nicolau e Natanael",
resposta2: "Belchior, Gaspar e Baltazar",
resposta3: "Belchior, Gaspar e Nataniel",
resposta4: "Gabriel, Benjamim e Melchior",
resposta5: "Melchior, Noé e Galileu",



respostacerta: "Belchior, Gaspar e Baltazar"},   {     pergunta: "Quais os principais heterônimos de Fernando Pessoa?",

resposta1: "Alberto Caeiro, Ricardo Reis e Álvaro de Campos",
resposta2: "Ariano Suassuna, Raul Bopp e Quincas Borba",
resposta3: "Bento Teixeira, Ricardo Reis e Haroldo de Campos",
resposta4: "Alberto Caeiro, Ricardo Leite e Augusto de Campos",
resposta5: "Bento Teixeira, Ricardo Reis e Augusto de Campos",


respostacerta: "Alberto Caeiro, Ricardo Reis e Álvaro de Campos"},   {     pergunta: "Qual a religião monoteísta que conta com o maior número de adeptos no mundo?",

resposta1: "Judaísmo",
resposta2: "Zoroastrismo",
resposta3: "Islamismo",
resposta4: "Cristianismo",
resposta5: "Hinduísmo",


respostacerta: "Cristianismo"},   {     pergunta: "Qual desses filmes foi baseado na obra de Shakespeare?",

resposta1: "Muito Barulho por Nada (2012)",
resposta2: "Capitães de Areia (2011)",
resposta3: "A Dama das Camélias (1936)",
resposta4: "A Revolução dos Bichos (1954)",
resposta5: "Excalibur (1981)",


respostacerta: "Muito Barulho por Nada (2012)"},   {     pergunta: "Quem foi o primeiro homem a pisar na Lua? Em que ano aconteceu?",

resposta1: "Yuri Gagarin, em 1961",
resposta2: "Buzz Aldrin, em 1969",
resposta3: "Charles Conrad, em 1969",
resposta4: "Charles Duke, em 1971",
resposta5: "Neils Armstrong, em 1969.",


respostacerta: "Neils Armstrong, em 1969."},   {     pergunta: "Qual o nome do cientista que descobriu o processo de pasteurização e a vacina contra a raiva?",

resposta1: "Marie Curie",
resposta2: "Blaise Pascal",
resposta3: "Louis Pasteurs",
resposta4: "Antoine Lavoisier",
resposta5: "Charles Darwin",


respostacerta: "Louis Pasteurs"},   {     pergunta: "As pessoas de qual tipo sanguíneo são consideradas doadores universais?",

resposta1: "Tipo A",
resposta2: "Tipo B",
resposta3: "Tipo O",
resposta4: "Tipo AB",
resposta5: "Tipo ABO",


respostacerta: "Tipo O"
}
];



export default Produtos