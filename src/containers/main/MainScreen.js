import * as React from "react";
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from "react-native";

import RoundedButton from "../../components/RoundedButton";
import ApiAuth from '../../api/ApiAuth'; 
import { AdMobInterstitial } from 'react-native-google-admob';

export default class App extends React.Component {
  state = {
    value: null,
    key: 0,
    options: 0,
    qtde: 9,
    qtde_calculo: 10,
    certas: 0,
    erradas: 0,
    percent: 0,
    // options: [],
    status: false
  };

  componentDidMount = () => {
    this.loadUser();
    this.listMyAdsFinished();
    
  };

  getMovies () {
    AdMobInterstitial.setAdUnitID('ca-app-pub-9534097551670907/6209331342');
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
}

  listMyAdsFinished = () => {
    var _this = this;
    console.log("listMyAdsFinished")
    ApiAuth.create(this.state.access_token).listCategories().then((response) => {
        //  console.warn(response);
        // console.log(response);
        if(response.data !== 'undefined' && response.data !== null  ) {
            _this.setState({ options : response.data })
        }
    }).catch(function (error) {
        console.log(error)
    })
}

  loadUser = () => {
    
    // let options = require("./Produtos");

     var Produtos;

    function shuffle(arra1) {
      var ctr = arra1.length,
        temp,
        index;

      // While there are elements in the array
      while (ctr > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * ctr);
        // Decrease ctr by 1
        ctr--;
        // And swap the last element with it
        temp = arra1[ctr];
        arra1[ctr] = arra1[index];
        arra1[index] = temp;
      }
      return arra1;
    }

    Produtos = shuffle(this.state.options);
    // Produtos = Produtos.slice(0, 10);
    
    this.setState({ options: Produtos });
    
  };

  checkAnswer = value => {
    if (!value) {
      alert("Escolha uma opção");
      return false;
    }

    if (value === this.state.options[this.state.key].respostacerta) {
      this.setState({ certas: this.state.certas + 1 });
    } else {
      this.setState({ erradas: this.state.erradas + 1 });
    }
    // console.warn(this.state.certas,"=>",this.state.erradas)
    this.setState({ value: null, key: this.state.key + 1 });
  };

  finish = async value => {
    
    if (!value) {
      alert("Escolha uma opção");
      return false;
    }

    if (value === this.state.options[this.state.key].respostacerta) {
      this.setState({ certas: this.state.certas + 1 });
    } else {
      this.setState({ erradas: this.state.erradas + 1 });
    }

    this.setState({ status: true });
  };

  reset = async () => {
    this.getMovies();
    this.setState({ key: 0, certas: 0, erradas: 0, status: false });
    this.loadUser();
  };

  render() {
    const { value } = this.state;
    const { key } = this.state;
    const { qtde } = this.state;
    const { status } = this.state;
    const { options } = this.state;

    if (options[key]) {
      
      if (status == false) {
        return (
          
          <View style={styles.container}>
            <View style={styles.textContainer}>
              <Text style={styles.textQuestion}>{options[key].pergunta}</Text>
            </View>
            <View style={styles.buttonsContainer}>
              {/* PERGUNTA 1 */}
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => {
                    this.setState({
                      value: options[key].resposta1
                    });
                  }}
                >
                  {value === options[key].resposta1 && (
                    <View style={styles.checkedCircle} />
                  )}
                </TouchableOpacity>
                <Text>{options[key].resposta1}</Text>
              </View>

              {/* PERGUNTA 2 */}
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => {
                    this.setState({
                      value: options[key].resposta2
                    });
                  }}
                >
                  {value === options[key].resposta2 && (
                    <View style={styles.checkedCircle} />
                  )}
                </TouchableOpacity>
                <Text>{options[key].resposta2}</Text>
              </View>

              {/* PERGUNTA 3 */}
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => {
                    this.setState({
                      value: options[key].resposta3
                    });
                  }}
                >
                  {value === options[key].resposta3 && (
                    <View style={styles.checkedCircle} />
                  )}
                </TouchableOpacity>
                <Text>{options[key].resposta3}</Text>
              </View>

              {/* PERGUNTA 4 */}
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => {
                    this.setState({
                      value: options[key].resposta4
                    });
                  }}
                >
                  {value === options[key].resposta4 && (
                    <View style={styles.checkedCircle} />
                  )}
                </TouchableOpacity>
                <Text>{options[key].resposta4}</Text>
              </View>

              {/* PERGUNTA 5 */}
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={styles.circle}
                  onPress={() => {
                    this.setState({
                      value: options[key].resposta5
                    });
                  }}
                >
                  {value === options[key].resposta5 && (
                    <View style={styles.checkedCircle} />
                  )}
                </TouchableOpacity>
                <Text>{options[key].resposta5}</Text>
              </View>
            </View>

            {key < qtde ? (
              <RoundedButton
                title="Avançar"
                onPress={() => {
                  this.checkAnswer(value);
                }}
                styles={styles.buttonStyle}
              />
            ) : (
              <RoundedButton
                title="Finalizar"
                onPress={() => {
                  this.finish(value);
                }}
                styles={styles.buttonStyleFinish}
              />
            )}
          </View>
        );
      } else {
        let percent;
        percent = (this.state.certas / this.state.qtde_calculo) * 100;
        return (
          <ScrollView>
          <View style={styles.textContainerFinish}>
            <Text style={styles.textQuestion}>SEU RESULTADO</Text>
            <Text
              style={{
                fontSize: 24,
                fontWeight: "bold",
                color: "#6068B1",
                marginBottom: 10,
                marginTop: 10
              }}
            >
              {percent.toFixed(2)} %
            </Text>

            <Text style={{ fontSize: 18, fontWeight: "bold", color: "green" }}>
              Certas: {this.state.certas}
            </Text>
            <Text style={{ fontSize: 18, fontWeight: "bold", color: "red" }}>
              Erradas: {this.state.erradas}
              {"\n"}
            </Text>
            <Text style={{ fontWeight: "bold", marginBottom: 5 }}>
              RESPOSTAS ABAIXO
            </Text>
            {options.map(item => {
              return (
                <View style={{ marginBottom: 5 }}>
                  <Text>
                    {item.pergunta}{" "}
                    <Text style={{ fontWeight: "bold" }}>
                      Resposta: {item.respostacerta}
                    </Text>
                  </Text>
                </View>
              );
            })}
            <RoundedButton
              title="Recomeçar"
              onPress={() => {
                this.reset();
              }}
              styles={styles.buttonStyleFinish}
            />
          </View>
          </ScrollView>
        );
      }
    }else{
      return(<View></View>);
    }
  }
}

const styles = StyleSheet.create({
  textContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  textContainerFinish: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 30
  },
  buttonsContainer: {
    padding: 20
  },
  textQuestion: {
    fontSize: 18,
    color: "#794F9B",
    fontWeight: "bold",
  },
  buttonStyleFinish: {
    width: "100%",
    height: 50,
    marginTop: 10,
    borderRadius: 10,
    backgroundColor: "#ffa500"
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ACACAC",
    marginRight: 10
  },

  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: "#794F9B",
    marginLeft: 2,
    marginTop: 2
  }
});
