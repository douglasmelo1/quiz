import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    centerStyle: {
      fontSize: 17,
      color: '#fff'
    },
    containerStyle: {
      backgroundColor: '#6068B1',
      justifyContent: 'space-around'
    },

})