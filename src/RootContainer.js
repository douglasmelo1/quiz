import React, {Component} from 'react';
import { StyleSheet, View, AppRegistry } from "react-native";
import { NativeRouter, Route } from "react-router-native";

import MainScreen from "./containers/main/MainScreen"; 
// import DetailScreen from "./containers/main/DetailScreen"; 

export default class RootContainer extends Component<Props> {

  render() {
    return (
      <NativeRouter>
      <View style={styles.container}>
        <Route exact path="/" component={MainScreen} />
      </View>
    </NativeRouter>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    
  }
});
